package javaFX.controllers;

import http.client.RunHttpRequestSingleton;
import javaFX.helper.classes.Constants;
import javaFX.helper.classes.HttpReqMethod;
import javaFX.helper.classes.MoveConditions;
import javafx.animation.AnimationTimer;
import javafx.animation.RotateTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * Created by Andrei-Mihai Nagy on 07.07.2017.
 */
public class MainWindowController
{
    /**
     * Helper Enum to know the direction the car is currently headed. Helps with moving the car icon in the window.
     */
    enum CarFacing
    {
        NORTH, EAST, SOUTH, WEST;

        CarFacing()
        {
            int order = ordinal();
        }
    }

    public ButtonBar buttonBarTop;
    public Button helpButton;
    public Button parkHereButton;
    public Button resetButton;
    public AnchorPane anchorPane;
    public Group groupParkingLot;
    public ImageView carImageView;
    public ImageView omilabImageView;

    private CarFacing carFacing = CarFacing.EAST;
    private Alert alert = null;

    // PRIVATE METHODS -------------------------------------------------------------------------------------------------

    /**
     * Load the OMiLAB logo and the car icon.
     * Position the car icon at the beginning.
     */
    private void setImages()
    {
        omilabImageView.setImage(new Image(Constants.OMiLAB_LOGO_URL));
        omilabImageView.setPreserveRatio(true);
        omilabImageView.setFitHeight(Constants.OMiLAB_LOGO_HEIGHT);

        carImageView.setImage(new Image(Constants.CAR_ICON_URL));
        carImageView.setPreserveRatio(true);
        carImageView.setFitHeight(Constants.CAR_ICON_SIZE);
        carImageView.setRotate(90);
        carImageView.relocate(15, Constants.PARKING_LOT_SIZE - Constants.CAR_ICON_SIZE);
        MoveConditions.startingLocation = getCurrentCarLocation();
    }

    /**
     * Method that creates a new window of type APPLICATION_MODAL (i.e. pop-up).
     * In this window the user need to enter their authentication token from OMiLAB.
     * If the tokan is invalid the user cannot continue.
     * It is implemented to be created in a new thread, to give the main window the opportunity to appear.
     */
    private void showAuthenticationTokenDialog(Stage stage)
    {
        Platform.runLater(() ->
        {
            try
            {
                Parent root = new FXMLLoader().load(this.getClass().getResource(Constants.AUTHENTICATON_WINDOW_PATH).openStream());
                Scene scene = new Scene(root);

                stage.setTitle("Authentication");
                stage.setScene(scene);
                stage.setResizable(false);
                stage.getIcons().add(new Image(Constants.AUTH_ICON_URL)); // top left window icon

                // When x is clicked terminate program
                stage.setOnCloseRequest(event -> ((Stage) helpButton.getScene().getWindow()).close());

                // Set stage modality as APPLICATION_MODAL (pop-up)
                stage.initModality(Modality.APPLICATION_MODAL);

                stage.showAndWait();
            } catch (IOException exception)
            {
                exception.printStackTrace();
                ((Stage) helpButton.getScene().getWindow()).close();
            }
        });
    }

    /**
     * Turns the car icon either clockwise or counter-clockwise by 90 degrees, depending on which variable in class MoveConditions is set true.
     */
    private void turnLeftOrRight90Degrees()
    {
        RotateTransition rotateTransition = new RotateTransition(Duration.millis(Constants.MILLIS_FOR_90DEG_TURN), carImageView);
        rotateTransition.setOnFinished(event -> MoveConditions.moving = false);

        if (MoveConditions.getTurnRight90Degrees())
            if (!MoveConditions.rotated)
            {
                MoveConditions.moving = true;

                rotateTransition.setByAngle(90);
                rotateTransition.play();

                MoveConditions.rotated = true;
                carFacing = CarFacing.values()[(carFacing.ordinal() == 3) ? 0 : (carFacing.ordinal() + 1)];
            }

        if (MoveConditions.getTurnLeft90Degrees())
            if (!MoveConditions.rotated)
            {
                MoveConditions.moving = true;

                rotateTransition.setByAngle(-90);
                rotateTransition.play();

                MoveConditions.rotated = true;
                carFacing = CarFacing.values()[(carFacing.ordinal() == 0) ? 3 : (carFacing.ordinal() - 1)];
            }
    }

    /**
     * Method executed if the variable parkYourself in the MoveConditions class is set true.
     * i.e. if the user pressed the key "P" when the program was in its initial state.
     */
    private void parkYourself()
    {
        if (!MoveConditions.moving)
            if (MoveConditions.selfParkingStatus == -1) // Initial program state (not started yet). Car icon needs to move to the right edge of the window.
            {
                MoveConditions.setMoveForwards();
                MoveConditions.moving = true;
                MoveConditions.selfParkingStatus = 1;
            } else if (MoveConditions.selfParkingStatus == 1) // Car icon got to the right edge of the window and now turns left.
            {
                MoveConditions.setTurnLeft90Degrees();
                MoveConditions.rotated = false;
                MoveConditions.selfParkingStatus = 2;
            } else if (MoveConditions.selfParkingStatus == 2) // Car icon turned left and now moves to the parking lot entrance.
            {
                MoveConditions.setMoveForwardsUntilParkingEntrance(); // Condition that is checked in the moveCarTo(x,y) function
                MoveConditions.moving = true;
                MoveConditions.selfParkingStatus = 3;
            } else if (MoveConditions.selfParkingStatus == 3) // Car icon got to the entrance and turns left again.
            {
                MoveConditions.setTurnLeft90Degrees();
                MoveConditions.rotated = false;
                MoveConditions.selfParkingStatus = 4;
            } else if (MoveConditions.selfParkingStatus == 4) // Car icon now faces left (west) and waits to see where the mBot finds a free parking spot.
            {
                if (MoveConditions.currentMBotParkingSpot == -1 && MoveConditions.counter < 7)
                    try
                    {
                        trySleep(2000);
                        RunHttpRequestSingleton runHttpRequest = RunHttpRequestSingleton.getInstance();
                        runHttpRequest.start(HttpReqMethod.GET, Constants.URL_PARKED_IN_SPOT);
                        MoveConditions.currentMBotParkingSpot = Integer.valueOf(runHttpRequest.getResponseMessage());
                        MoveConditions.counter++;
                        if (isBetween(1, MoveConditions.currentMBotParkingSpot, 6))
                        {
                            MoveConditions.PIXEL_SPEED = 4;
                            MoveConditions.setMoveForwards();
                            MoveConditions.moving = true;
                            MoveConditions.selfParkingStatus = 5;
                        }
                    } catch (NumberFormatException ignored)
                    {
                    }
                else // mBot found no free parking spot. Car icon moves forwards until the end of the parking lot.
                {
                    MoveConditions.setMoveForwards();
                    MoveConditions.moving = true;
                    MoveConditions.PIXEL_SPEED = 4;
                    MoveConditions.selfParkingStatus = 8;
                }
            } else if (MoveConditions.selfParkingStatus == 5) // mBot found a free parking spot and parked itself. Now the JavaFX car icon moves to the right spot in the parking place according to the input from the mBot.
            {
                if (MoveConditions.currentMBotParkingSpot == 4 || MoveConditions.currentMBotParkingSpot == 5 || MoveConditions.currentMBotParkingSpot == 6)
                {
                    MoveConditions.setTurnLeft90Degrees();
                    MoveConditions.rotated = false;
                }
                if (MoveConditions.currentMBotParkingSpot == 1 || MoveConditions.currentMBotParkingSpot == 2 || MoveConditions.currentMBotParkingSpot == 3)
                {
                    MoveConditions.setTurnRight90Degrees();
                    MoveConditions.rotated = false;
                }
                MoveConditions.selfParkingStatus = 6;
            } else if (MoveConditions.selfParkingStatus == 6) // Car icon turns either left or right depending on mBot input and moves in the right place.
            {
                if (MoveConditions.currentMBotParkingSpot == 1 || MoveConditions.currentMBotParkingSpot == 2 || MoveConditions.currentMBotParkingSpot == 3)
                {
                    MoveConditions.setMoveForwards();
                    MoveConditions.moving = true;
                }
                if (MoveConditions.currentMBotParkingSpot == 4 || MoveConditions.currentMBotParkingSpot == 5 || MoveConditions.currentMBotParkingSpot == 6)
                {
                    MoveConditions.setMoveForwardsUntilParked();
                    MoveConditions.moving = true;
                }
                MoveConditions.selfParkingStatus = 7; // FINISHED PARKING ITSELF (end state)
                // Show a pop-up that informs the user that there are no free parking spaces.
                Platform.runLater(() ->
                {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Info");
                    alert.setHeaderText("Success.");
                    alert.setContentText(null);
                    Button okButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                    okButton.setDisable(false);
                    okButton.setText("EXIT");
                    okButton.setOnAction(event -> ((Stage) helpButton.getScene().getWindow()).close());
                    alert.showAndWait();
                });
            } else if (MoveConditions.selfParkingStatus == 8) // Car icon turns right by 90 degrees.
            {
                MoveConditions.setTurnRight90Degrees();
                MoveConditions.rotated = false;
                MoveConditions.selfParkingStatus = 9;
            } else if (MoveConditions.selfParkingStatus == 9) // Car icon turns right by 90 degrees again for a full 180 degree turn.
            {
                MoveConditions.setTurnRight90Degrees();
                MoveConditions.rotated = false;
                MoveConditions.selfParkingStatus = 7; // FINISHED PARKING ITSELF (end state)

                // Show a pop-up that informs the user that there are no free parking spaces.
                Platform.runLater(() ->
                {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Info");
                    alert.setHeaderText("The mBot found no free parking spot (i.e. all parking spaces are taken).");
                    alert.setContentText(null);
                    Button okButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                    okButton.setDisable(false);
                    okButton.setText("EXIT");
                    okButton.setOnAction(event -> ((Stage) helpButton.getScene().getWindow()).close());
                    alert.showAndWait();
                });
            }
    }

    /**
     * Method executes if either the variable moveForwards or the variable moveForwardsUntilParkingEntrance in the MoveConditions class is set true.
     */
    private void moveForwards()
    {
        int dx = 0, dy = 0;
        switch (carFacing) // To check which way is up, i.e. which direction the car icon should correctly move.
        {
            case NORTH:
                dy -= MoveConditions.PIXEL_SPEED;
                break;
            case SOUTH:
                dy += MoveConditions.PIXEL_SPEED;
                break;
            case EAST:
                dx += MoveConditions.PIXEL_SPEED;
                break;
            case WEST:
                dx -= MoveConditions.PIXEL_SPEED;
                break;
        }
        moveCarBy(dx, dy);
    }

    /**
     * Method executes if the variable moveBackwards in the MoveConditions class is set true.
     */
    private void moveBackwards()
    {
        int dx = 0, dy = 0;
        switch (carFacing) // To check which way is up, i.e. which direction the car icon should correctly move.
        {
            case NORTH:
                dy += MoveConditions.PIXEL_SPEED;
                break;
            case SOUTH:
                dy -= MoveConditions.PIXEL_SPEED;
                break;
            case EAST:
                dx -= MoveConditions.PIXEL_SPEED;
                break;
            case WEST:
                dx += MoveConditions.PIXEL_SPEED;
                break;
        }
        moveCarBy(dx, dy);
    }

    /**
     * The current location is calculated using the centre point of the car icon.
     * X and Y coordinates for the car centre point will be equal (Constants.CAR_ICON_SIZE/2) because the car icon is square.
     *
     * @return A Point containing the (x, y) coordinates of the cars current location (measured in pixels).
     */
    private Point getCurrentCarLocation()
    {
        int x = new Double(Constants.CAR_ICON_SIZE / 2 + carImageView.getLayoutX()).intValue();
        int y = new Double(Constants.CAR_ICON_SIZE / 2 + carImageView.getLayoutY()).intValue();
        return new Point(x, y);
    }

    /**
     * Checks if car icon is in one of the 6 parking spots.
     *
     * @param currCarLocation Current location of the car icon on the screen.
     * @return True if it yes, false if the car is somewhere else on the screen.
     */
    private boolean carIsInParkingSpot(Point currCarLocation)
    {
        boolean carIn101 = isBetween(96, currCarLocation.y, 116) && isBetween(95, currCarLocation.x, 145);
        if (carIn101)
        {
            MoveConditions.currentJavaFXParkingSpot = 1;
            return true;
        }
        boolean carIn102 = isBetween(96, currCarLocation.y, 116) && isBetween(225, currCarLocation.x, 295);
        if (carIn102)
        {
            MoveConditions.currentJavaFXParkingSpot = 2;
            return true;
        }
        boolean carIn103 = isBetween(96, currCarLocation.y, 116) && isBetween(380, currCarLocation.x, 460);
        if (carIn103)
        {
            MoveConditions.currentJavaFXParkingSpot = 3;
            return true;
        }
        boolean carIn104 = isBetween(455, currCarLocation.y, 475) && isBetween(95, currCarLocation.x, 145);
        if (carIn104)
        {
            MoveConditions.currentJavaFXParkingSpot = 4;
            return true;
        }
        boolean carIn105 = isBetween(455, currCarLocation.y, 475) && isBetween(225, currCarLocation.x, 295);
        if (carIn105)
        {
            MoveConditions.currentJavaFXParkingSpot = 5;
            return true;
        }
        boolean carIn106 = isBetween(455, currCarLocation.y, 475) && isBetween(380, currCarLocation.x, 460);
        if (carIn106)
        {
            MoveConditions.currentJavaFXParkingSpot = 6;
            return true;
        }

        MoveConditions.currentJavaFXParkingSpot = -1;
        return false;
    }

    /**
     * number between lowerLimit and upperLimit - i.e. number is in the interval [lowerLimit, upperLimit]
     */
    private boolean isBetween(int lowerLimit, int number, int upperLimit)
    {
        return lowerLimit <= number && number <= upperLimit;
    }

    private void moveCarBy(int dx, int dy)
    {
        if (dx == 0 && dy == 0) return;

        int x = getCurrentCarLocation().x + dx;
        int y = getCurrentCarLocation().y + dy;

        moveCarTo(x, y);
    }

    private void moveCarTo(double x, double y)
    {
        // x and y coordinates are the same for the centre point because the car icon is a square
        final double centrePoint_carIcon = Constants.CAR_ICON_SIZE / 2;

        // for the self parking case, so that the car image moves until max. the parking lot entrance
        if (MoveConditions.getMoveForwardsUntilParkingEntrance())
            if (!(y - centrePoint_carIcon - 200 >= 0))
            {
                MoveConditions.stopCar();
                MoveConditions.moving = false;
                return;
            }

        // for the self parking case, so that the car image moves down until the car is in the parking spot
        if (MoveConditions.getMoveForwardsUntilParked())
            if (!(y + centrePoint_carIcon <= Constants.PARKING_LOT_SIZE - 170))
            {
                MoveConditions.stopCar();
                MoveConditions.moving = false;
                return;
            }

        // for the self parking case, so that the car image moves in the parking lot until it reaches spots 106 or 103
        if (MoveConditions.currentMBotParkingSpot == 6 || MoveConditions.currentMBotParkingSpot == 3)
            if (!(x - centrePoint_carIcon - 340 >= 0))
            {
                MoveConditions.stopCar();
                MoveConditions.moving = false;
                return;
            }

        // for the self parking case, so that the car image moves in the parking lot until it reaches spots 105 or 102
        if (MoveConditions.currentMBotParkingSpot == 5 || MoveConditions.currentMBotParkingSpot == 2)
            if (!(x - centrePoint_carIcon - 190 >= 0))
            {
                MoveConditions.stopCar();
                MoveConditions.moving = false;
                return;
            }

        // +/- 15 pixels margin so that the car does not get all the way to the top/left/right edge (bottom not, because there is not enough space)
        if (x - centrePoint_carIcon - 15 >= 0 // left edge condition (car icon should not be moved outside the window)
                && x + centrePoint_carIcon + 15 <= Constants.PARKING_LOT_SIZE // right edge condition (car icon should not be moved outside the window)
                && y - centrePoint_carIcon - 15 >= 0 // top edge condition (car icon should not be moved outside the window)
                && y + centrePoint_carIcon <= Constants.PARKING_LOT_SIZE) // bottom edge condition (car icon should not be moved outside the window)
            carImageView.relocate(x - centrePoint_carIcon, y - centrePoint_carIcon);
        else // car arrives to the edge of the window --> stop both in javafx and the mBot
        {
            MoveConditions.stopCar();
            MoveConditions.moving = false;
        }
    }

    /**
     * Method executed if the variable parkHereButtonClicked in the MoveConditions class is set true.
     * i.e. if the user brings the modelled car to a parking space and clicks on the 'Park here' button that appears in the top right corner.
     */
    private void parkYourselfInSpot()
    {
        try
        {
            trySleep(3000);
            RunHttpRequestSingleton runHttpRequest = RunHttpRequestSingleton.getInstance();
            runHttpRequest.start(HttpReqMethod.GET, Constants.URL_PARKED_IN_SPOT);
            MoveConditions.currentMBotParkingSpot = Integer.valueOf(runHttpRequest.getResponseMessage());

            if (MoveConditions.currentMBotParkingSpot == -2 || isBetween(1, MoveConditions.currentMBotParkingSpot, 6))
            {
                MoveConditions.parkHereButtonClicked = false;
                if (alert.isShowing())
                {
                    if (MoveConditions.currentMBotParkingSpot == -2)
                    {
                        Platform.runLater(() -> // Show error alert in new thread. (parking spot is already taken)
                        {
                            alert.close();
                            alert.setTitle("Info");
                            alert.setHeaderText("Oops. Looks like the parking spot you chose is already taken!");
                            alert.setContentText(null);
                            alert.setGraphic(new ImageView(new Image(Constants.FAILURE_ICON_URL, 30, 30, true, true)));
                            Button okButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                            okButton.setText("EXIT");
                            okButton.setDisable(false);
                            okButton.setOnAction(event ->
                            {
                                MoveConditions.reset();
                                ((Stage) helpButton.getScene().getWindow()).close();
                            });
                            alert.showAndWait();
                        });
                    } else Platform.runLater(() -> // Show success thread in new thread.
                    {
                        alert.close();
                        alert.setTitle("Info");
                        alert.setHeaderText("Success!");
                        alert.setContentText(null);
                        alert.setGraphic(new ImageView(new Image(Constants.SUCCESS_ICON_URL, 30, 30, true, true)));
                        Button okButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
                        okButton.setDisable(false);
                        okButton.setOnAction(event -> ((Stage) helpButton.getScene().getWindow()).close());
                        okButton.setText("EXIT");
                        alert.showAndWait();
                    });
                }
            }
        } catch (NumberFormatException | NullPointerException ignored)
        {
        }
    }

    /**
     * Method opens in default browser the URI passed as string parameter.
     *
     * @param uri URI to be opened in the default browser.
     */
    private void openInBrowser(String uri)
    {
        try
        {
            Desktop.getDesktop().browse(new URI(uri));
        } catch (IOException | URISyntaxException e1)
        {
            e1.printStackTrace();
        }
    }

    /**
     * Simple sleep function
     */
    private void trySleep(int ms)
    {
        try
        {
            Thread.sleep(ms);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    // PUBLIC METHODS --------------------------------------------------------------------------------------------------

    @FXML
    public void initialize()
    {
        // Load the OMiLAB logo and the car icon.
        // Position the car icon at the beginning.
        setImages();

        // Show the authentication window as a new pop-up.
        showAuthenticationTokenDialog(new Stage());

        new AnimationTimer()
        {
            @Override
            public void handle(long now)
            {
                // Car icon needs to rotate either left or right by 90 degrees.
                if (MoveConditions.getTurnLeft90Degrees() || MoveConditions.getTurnRight90Degrees())
                    turnLeftOrRight90Degrees();

                // Variable parkYourself is set true = mBot will park itself on the first free spot. Car icon in JavaFX will update accordingly.
                if (MoveConditions.parkYourself)
                {
                    parkYourself();
                    resetButton.setDisable(true);
                }

                // Car icon needs to move forwards either until a stopping key is pressed, a window margin is arrived or it got to the parking lot entrance.
                if (MoveConditions.getMoveForwards() || MoveConditions.getMoveForwardsUntilParkingEntrance() || MoveConditions.getMoveForwardsUntilParked())
                    moveForwards();

                // Car icon needs to move backwards
                if (MoveConditions.getMoveBackwards())
                    moveBackwards();

                // Enable/disable parkHere button. ParkHere button is only enabled when the car icon is inside one of the 6 parking spots.
                if (!MoveConditions.parkYourself)
                {
                    Point currCarLocation = getCurrentCarLocation();
                    if (carIsInParkingSpot(currCarLocation))
                    {
                        parkHereButton.setDisable(false);
                        parkHereButton.setVisible(true);
                    } else
                    {
                        parkHereButton.setDisable(true);
                        parkHereButton.setVisible(false);
                    }
                }

                // Variable parkHereButtonClicked is set true = mBot will park itself on the spot the user chose.
                if (MoveConditions.parkHereButtonClicked)
                    parkYourselfInSpot();
            }
        }.start();
    }

    /**
     * Method executed when the PARK HERE button on the start window is clicked.
     * A HTTP PUT request is sent to the server which will move the car accordingly.
     */
    public void parkHereButtonClicked()
    {
        MoveConditions.parkHereButtonClicked = true;

        RunHttpRequestSingleton runHttpRequest = RunHttpRequestSingleton.getInstance();
        runHttpRequest.start(HttpReqMethod.PUT, Constants.URL_PARK_IN_X + Integer.toString(MoveConditions.currentJavaFXParkingSpot));

        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText("Check out the mBot. It should pe moving.");
        alert.setContentText(null);
        alert.getDialogPane().lookupButton(ButtonType.OK).setDisable(true);
        alert.showAndWait();

        trySleep(15000);
    }

    /**
     * Method executed when the HELP button on the start window is clicked.
     * A pop-up window will appear with the instructions on driving the car in the GUI.
     */
    public void helpButtonClicked()
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Help");
        alert.setHeaderText("Instructions");
        alert.setContentText(Constants.HELP_TEXT);
        alert.setGraphic(new ImageView(new Image(Constants.OMiLAB_LOGO_URL, 100, 100, true, true)));

        // Get the Stage.
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        // Add a custom icon.
        stage.getIcons().add(new Image(Constants.HELP_ICON_URL));

        ButtonType buttonTypeLink = new ButtonType("Open project webpage", ButtonBar.ButtonData.OTHER);
        ButtonType buttonTypeCancel = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);

        alert.getButtonTypes().setAll(buttonTypeLink, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == buttonTypeLink)
            openInBrowser(Constants.URL_PROJECT_WEBPAGE);
    }

    /**
     * Method executed when the RESET button on the start window is clicked.
     * Resets the window to its initial state.
     */
    public void resetButtonClicked()
    {
        MoveConditions.reset();
        carImageView.setImage(new Image(Constants.CAR_ICON_URL));
        carImageView.setPreserveRatio(true);
        carImageView.setFitHeight(Constants.CAR_ICON_SIZE);
        carImageView.setRotate(90);
        carImageView.relocate(15, Constants.PARKING_LOT_SIZE - Constants.CAR_ICON_SIZE);
        carFacing = CarFacing.EAST;
    }

    /**
     * Method executed when the OMiLAB image is clicked.
     * A new tab in the default browser is opened to the OMiLAB homepage.
     */
    public void omilabImageViewClicked()
    {
        openInBrowser(Constants.URL_OMiLAB_HOMEPAGE);
    }
}
