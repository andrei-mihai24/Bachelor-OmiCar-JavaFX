package javaFX.helper.classes;

import java.awt.*;

/**
 * Created by Andrei-Mihai Nagy on 07.07.2017.
 */
public class MoveConditions
{
    // Private static variables, because only one of them can be true at any given time.
    // They are set to true in the setters. When one is set to true, all others are set to false.
    static private boolean moveForwards = false;
    static private boolean moveForwardsUntilParkingEntrance = false;
    static private boolean moveForwardsUntilParked = false;
    static private boolean moveBackwards = false;
    static private boolean turnLeft90Degrees = false;
    static private boolean turnRight90Degrees = false;

    static public String authToken = ""; // the token from the omilab project webpage. Without it the HTTP Get requests will not be successful.
    static public int counter = 0; // saves how many times the URL_PARKED_IN_SPOT HTTP Get request is sent.
    static public int PIXEL_SPEED = 2; // the "speed" the JavaFX car icon is moving.

    static public int currentJavaFXParkingSpot = -1; // the current parking spot of the modelled car icon (-1 means outside, 1 through 6 means in one of them)
    static public int currentMBotParkingSpot = -1; // the current parking spot of the real mBot robot car (-1 means outside, 1 through 6 means in one of them)
    static public boolean rotated = false; // variable that helps remember if a 90 degree turn has already been made, in order to stop the car from turning too many times.
    static public boolean moving = false; // variable that remembers if the car is currently moving (to stop other actions, such as turning)
    static public Point startingLocation = null; // variable that saves the x and y coordinates of the starting location of the car
    static public boolean carHasLeftInitialState = false; // variable that remembers if the modelled car icon has left its initial state (location) - e.g. in order to stop certain actions
    static public boolean parkHereButtonClicked = false; // variable that remembers if the parkHereButton was clicked in order to start the self-parking in user chosen spot use case.

    // -1 = not started (initial state)
    // 1 = first part done (go to the right edge)
    // 2 = second part done (turn left 90 deg)
    // 3 = third part done (go to the parking lot entrance)
    // 4 = fourth part done (turn left 90 deg)
    // 5 = fifth part done (go in front of the right parking spot depending on mBot input)
    // 6 = sixth part done (turn left/right depending on mBot input)
    // 7 = finished parking itself (end state)
    // 8 = no free parking spot found. Move until the end of the parking lot
    // 9 = turn 180 degrees
    static public int selfParkingStatus = -1;

    static public boolean parkYourself = false;// variable that remembers if the parkYourself use case was initiated (by pressing 'P' on keyboard) and currently running.


    public static boolean getMoveForwards()
    {
        return moveForwards;
    }

    public static boolean getMoveForwardsUntilParkingEntrance()
    {
        return moveForwardsUntilParkingEntrance;
    }

    public static boolean getMoveForwardsUntilParked()
    {
        return moveForwardsUntilParked;
    }

    public static boolean getMoveBackwards()
    {
        return moveBackwards;
    }

    public static boolean getTurnLeft90Degrees()
    {
        return turnLeft90Degrees;
    }

    public static boolean getTurnRight90Degrees()
    {
        return turnRight90Degrees;
    }

    public static void setMoveForwards()
    {
        MoveConditions.moveForwards = true;
        MoveConditions.moveForwardsUntilParked = MoveConditions.moveForwardsUntilParkingEntrance = MoveConditions.moveBackwards = MoveConditions.turnRight90Degrees = MoveConditions.turnLeft90Degrees = false;
    }

    public static void setMoveForwardsUntilParkingEntrance()
    {
        MoveConditions.moveForwardsUntilParkingEntrance = true;
        MoveConditions.moveForwardsUntilParked = MoveConditions.moveForwards = MoveConditions.moveBackwards = MoveConditions.turnRight90Degrees = MoveConditions.turnLeft90Degrees = false;
    }

    public static void setMoveForwardsUntilParked()
    {
        MoveConditions.moveForwardsUntilParked = true;
        MoveConditions.moveForwardsUntilParkingEntrance = MoveConditions.moveForwards = MoveConditions.moveBackwards = MoveConditions.turnRight90Degrees = MoveConditions.turnLeft90Degrees = false;
    }

    public static void setMoveBackwards()
    {
        MoveConditions.moveBackwards = true;
        MoveConditions.moveForwardsUntilParked = MoveConditions.moveForwardsUntilParkingEntrance = MoveConditions.moveForwards = MoveConditions.turnRight90Degrees = MoveConditions.turnLeft90Degrees = false;
    }

    public static void setTurnLeft90Degrees()
    {
        MoveConditions.turnLeft90Degrees = true;
        MoveConditions.moveForwardsUntilParked = MoveConditions.moveForwardsUntilParkingEntrance = MoveConditions.moveBackwards = MoveConditions.turnRight90Degrees = MoveConditions.moveForwards = false;
    }

    public static void setTurnRight90Degrees()
    {
        MoveConditions.turnRight90Degrees = true;
        MoveConditions.moveForwardsUntilParked = MoveConditions.moveForwardsUntilParkingEntrance = MoveConditions.moveBackwards = MoveConditions.turnLeft90Degrees = MoveConditions.moveForwards = false;
    }

    public static void stopCar()
    {
        moveForwards = false;
        moveForwardsUntilParkingEntrance = false;
        moveForwardsUntilParked = false;
        moveBackwards = false;
        turnLeft90Degrees = false;
        turnRight90Degrees = false;
    }

    public static void reset()
    {
        counter = 0;
        PIXEL_SPEED = 1; // the "speed" the JavaFX car icon is moving

        moveForwards = false;
        moveForwardsUntilParkingEntrance = false;
        moveForwardsUntilParked = false;
        moveBackwards = false;
        turnLeft90Degrees = false;
        turnRight90Degrees = false;

        currentJavaFXParkingSpot = -1;
        currentMBotParkingSpot = -1;
        rotated = false;
        moving = false;
        startingLocation = null;
        carHasLeftInitialState = false;

        parkHereButtonClicked = false;

        // -1 = not started (initial state)
        // 1 = first part done (go to the right edge)
        // 2 = second part done (turn left 90 deg)
        // 3 = third part done (go to the parking lot entrance)
        // 4 = fourth part done (turn left 90 deg)
        // 5 = fifth part done (go in front of the right parking spot depending on mBot input)
        // 6 = sixth part done (turn left/right depending on mBot input)
        // 7 = finished parking itself (end state)
        selfParkingStatus = -1;
        parkYourself = false;
    }
}